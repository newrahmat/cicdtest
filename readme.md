# MicroService for Officer Module with Lumen PHP Framework

MicroService are a software development technique, a variant of the service-oriented architecture (SOA) structural style, that arranges an application as a collection of loosely coupled services. In a MicroService architecture, services are fine-grained and the protocols are lightweight ([WikiPedia](https://en.wikipedia.org/wiki/Microservices)). MicroService for officer module are MicroService that are used to process or manage officer data that is built with a Laravel Lumen. Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. Laravel Lumen believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Requirements

To use MicroService for Officer Module, you must ensure that you meet the following requirements:
- [Composer](https://getcomposer.org/)
- [PHP](https://www.php.net/) >= 7.2
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension

## Installation

To use MicroService for Officer Module, you must follow the steps below:
- Clone a Repository
```git
git clone https://MrAndreLoyalto@bitbucket.org/loyaltoid/korlantas-officer.git
```
- Grant Permission for bootstrap folder and storage folder (Linux)
```bash
chmod 777 -R bootstrap
```
```bash
chmod 777 -R storage
```
- Install Vendor
###### For Development
```composer
composer install --optimize-autoloader
```
###### For Production
```composer
composer install --optimize-autoloader --no-dev
```
- Create .env file from .env.example (Linux)
```bash
cp .env.example .env
```
- Configuring .env file
```bash
# Set APP_KEY with random string (length: 32 Characters)
APP_KEY=0Mc9pv1C87db6b1qCDhGtwMc6nJrwGQs
# Set APP_URl with your url
APP_URL=https://mrandreid.com/

# Set DB_CONNECTION with your database driver used (Ex: mysql; Option: sqlite (SQLite 3.8.8+), mysql (MySQL 5.6+), pgsql (PostgreSQL 9.4+), sqlsrv (SQL Server 2017+);)
DB_CONNECTION=mysql
# Set DB_HOST with your database host
DB_HOST=127.0.0.1
# Set DB_PORT with your database port
DB_PORT=3306
# Set DB_DATABASE with your database name
DB_DATABASE=yourDatabaseName
# Set DB_USERNAME with your database username
DB_USERNAME=yourDatabaseUsername
# Set DB_PASSWORD with your database password
DB_PASSWORD=yourDatabasePassword
```
###### For Production
```bash
# Set APP_ENV with production (Option: local (For Development), production (For Production))
APP_ENV=production
# Set APP_DEBUG with false
APP_DEBUG=false
```
- Migrate the Database
```php
php artisan migrate
```
- Seed the database (Optional)
```php
php artisan db:seed
```

## Usage

Run MicroService For Officer Module
```php
php -S your.url:8888 -t public
```

## Testing With PHPUnit

To test MicroService for Officer Module, you must follow the steps below:
- Reset the database and add records with the seeder
```php
php artisan migrate:fresh --seed
```
- Run MicroService For Officer Module
```php
php -S your.url:8888 -t public
```
- Run command for testing
```bash
vendor/bin/phpunit
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## Versioning

I use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository. 

## Authors

**Andrea Adam** - [MrAndreID](https://github.com/MrAndreID/)

## Official Documentation for Laravel Lumen

Documentation for Laravel Lumen can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities for Laravel Lumen

If you discover a security vulnerability within Laravel Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License for Laravel Lumen

The Laravel Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
