<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Roles', function (Blueprint $table) {
            $table->increments('RoleId');
            $table->string('RoleName', 255)->nullable();
        });
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Roles');
    }
}
