<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPermissionsTable extends Migration
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserPermissions', function (Blueprint $table) {
            $table->increments('UserPermissionId');
            $table->integer('UserId')->unsigned()->nullable();
            $table->foreign('UserId')->references('UserId')->on('Users');
            $table->string('PermissionKey', 100)->nullable();
        });
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserPermissions');
    }
}
