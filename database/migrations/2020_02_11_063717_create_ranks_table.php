<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRanksTable extends Migration
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ranks', function (Blueprint $table) {
            $table->increments('RankId');
            $table->string('RankCode', 255)->nullable();
            $table->string('RankName', 255)->nullable();
            $table->tinyInteger('RankStatus')->nullable()->comment('0:not active, 1:active, 6:other, 7:deleted');
        });
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ranks');
    }
}
