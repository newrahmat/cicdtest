<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionsTable extends Migration
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RolePermissions', function (Blueprint $table) {
            $table->increments('RolePermissionId');
            $table->integer('RoleId')->unsigned()->nullable();
            $table->foreign('RoleId')->references('RoleId')->on('Roles');
            $table->string('PermissionKey', 100)->nullable();
        });
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RolePermissions');
    }
}
