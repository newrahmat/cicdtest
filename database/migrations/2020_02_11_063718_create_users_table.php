<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table) {
            $table->increments('UserId');
            $table->string('Username', 100)->nullable();
            $table->string('Fullname', 100)->nullable();
            $table->string('NIK', 20)->nullable();
            $table->tinyInteger('NIKConfirmed')->nullable();
            $table->string('NRP', 20)->nullable()->comment('No. Registrasi Pokok');
            $table->string('NRPHead', 20)->nullable()->comment('No. Registrasi Pokok Atasan');
            $table->integer('RankId')->unsigned()->nullable();
            $table->foreign('RankId')->references('RankId')->on('Ranks');
            $table->string('Unit', 100)->nullable();
            $table->string('Phone', 20)->nullable();
            $table->tinyInteger('PhoneConfirmed')->nullable();
            $table->string('Email', 30)->nullable();
            $table->tinyInteger('EmailConfirmed')->nullable();
            $table->text('Picture')->nullable();
            $table->string('Source', 3)->nullable()->comment('registration source: app, web, adm');
            $table->string('PasswordHash', 100)->nullable();
            $table->text('PasswordSalt')->nullable();
            $table->string('PinHash', 100)->nullable();
            $table->tinyInteger('TwoFactorEnabled')->nullable()->comment('0:none, 1:otp, 2:email, 3:google authenticator, 4:microsoft authenticator');
            $table->tinyInteger('AccessFailedCount')->nullable();
            $table->string('SecurityStamp')->nullable();
            $table->string('InstallId')->nullable();
            $table->string('DeviceId')->nullable();
            $table->enum('DeviceType', ['android', 'ios'])->nullable();
            $table->tinyInteger('Status')->nullable()->comment('0:not active, 1:active, 2:blacklist, 3:suspend, 4:expired, 5:paid, 6:other, 7:deleted');
            $table->dateTime('RegisterDate')->nullable();
            $table->dateTime('LastUpdate')->nullable();
            $table->dateTime('ClosingDate')->nullable();
        });
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Users');
    }
}
