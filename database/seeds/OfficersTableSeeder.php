<?php

use Illuminate\Database\Seeder;
use App\UserPermission;
use App\User;

class OfficersTableSeeder extends Seeder
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $officer = new User;
        $officer->Username = 'MrAndreID';
        $officer->Fullname = 'Andrea Adam';
        $officer->NIK = '1234567890123456';
        $officer->NIKConfirmed = 1;
        $officer->NRP = '0987654321654321';
        $officer->NRPHead = '6543210987654321';
        $officer->RankId = 15;
        $officer->Unit = 'AU';
        $officer->Phone = '6281288795410';
        $officer->PhoneConfirmed = 1;
        $officer->Email = 'andre@loyalto.id';
        $officer->EmailConfirmed = 1;
        $officer->Picture = 'https://s3.loyalto.id/korsdev/imageprivate/7XCsVwZ11589452499-4086e59044a597b0255984ec4bb00ee0.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=v5VY3bM8CQkE%238L0mVcH%2F20200514%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200514T103459Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=676d7a87f22a9760f40fefeb87a57981573e158a555046d5b9c3ad57608f7ccf';
        $officer->Source = 'adm';
        $officer->PasswordHash = '$2y$10$VOpijnpB3Qoqtpish69v9udr62mV/3Sa8BJ2d7ScDetG8xtrmQ92i';
        $officer->PasswordSalt = 'eyJpdiI6IllzRmRnQ01HXC91ZUtLQVl1QXRMNWhRPT0iLCJ2YWx1ZSI6Im0xXC9jd0FvRVA2SFNxT1owMkNJS3JJaFlGdEZ1Y0t6XC9Bd3dQRTBJM1ZSMldaM2dDa05xWExmUmc5WkJxZjFZRnBkdVFFZ25aWkRsVHE5ZkdpTCtMRjFpK0ZCbmxDWTZKM0RlZVhickE2TmlTUzVSWnJSUGJ4Mzl5TU9VNGtTNk5pMUVoc05ZZHQ1UDdOOHVxNUh3QTl3a1pxYjRORFZRQXBQU0ZlXC9JTVMwbnhLRzMwbVRXS1RTY1ppRDZKbHBJZHluNVI2bXh5V1RGYldFbHltc2RwcHhabWpsZHllZ1VUVnpmXC9yM2RTZWlCaFwvemJjOFBqWnJKa09xTUZSVWFxN2gxYndNTnd4dHdRSXhteWpIdGJra1wvTUEzOTBTKzgxeEExSSthdHpGcDVFc3lKbWw4eVl6ZGVYVm1YdmhOWUZTK3g1VUtDdlp3WkpVRlFnSDd5aWRETDNkUlJMQStnU2FFTkljcnRJb2Z4anZIc0E1VkZjbHBpVnE3MDQ3SWZCUm8wcjhPemJjekQxMVwvS1ZmWTFacTdrSFU0NHViOXNlMnVnazhwS2VsSlNBcXlpMzRPclk3OXlyU3BcL1RRMGR5c3h3XC9SZmZxYVwvbzliXC9XczUrTjJ0bnVFc3BKQlpaakNWcGlpcW0waUcrWkRFRjN5VDN4V00xdVdPcU45K1ZyNXJZRm9ZNkxraTZWc2cxS0JUQXVTUjQ1NkdLanlcL2NleE9xUVVQYWtUTERKcHFqK1B2elNCUGdpaHNmeXRwMk1iZDJ3TFJXNEhLNDQ1RUtsT0s0bFJjUHhnZ25DTkhjRnE5ZlB1MmdrTkdrcmJDakI0ZGtVYWxOazFnYzRDOEwxZHZzcTdvRkxMXC9FK2FyNmJnVTd2RFNYdVlsUmhcLzNLSEg4Z3FiTjJhak9wOVRVdit2TnZjQk5CYkh3aVdUSDVpRFwvekFYVnhTUnN2dzRadGFENWhtYVpGUUJwdDNQYTNKdjNTdGtPQWsyQjNaV0JYVHBmOVwvU3FobDhIUERTQ1wvc0ozTit6Vys5T3JBQ3J1M0dZU3VHWENvajd6cmZvMmtlVWp1clFpQUV6aXNaRUJ6RjNKYjZvREZaYm96UmV4eDBSZ2NcL0wrNXhYTnYzSVo4ZHF1cXpHdHpya3pFUFBscU93ejV1bzVwbEIxOCtkMDk4VHhCMVp3ZWo5bEVST3FtUzZVTHpZWk5tS0hpdUROekNVTmw5OGlGNFR3aFhWd0REaDFvVlpkWGgxaEwwcE1jQkM3T0t6UHNKWmRFSWlJUWpIQlJ4ZHBEYWJXNkxWYzNrYklOT2pPUzNCZjZMMmJRVXpBVFhoR2tJelQrVE9hb0VXWnlcL2xiZkxzdk5RT294aGdVRXN2TWpvQzlBNGRnYkorMERmVnp4dzJwRTZIRXVLdjM2SnAxRG4zOFBcL0JSaDRwNGFOT1oyNTU1eDRWREZcL0liRGtnSzZPRFI0Y2tSNDlmeU5lOUNvVm4xNXVJS3d3QllWMUFtdGc1ZVhTbGpWc0ZCUFh6MlBXc3YxTWJrajR2OHBEUFFYVHNKVnJCODFVMlo5STVxY21KcFo0clk1ZDF1R1pWQWUrQ1wvVlFlZHVWMEZYSUk2SXlTR0UwYUZLbmd5bzNFcGFEd0xrV2l2NWRqRmR6OG5QWHY1NitLcFVkaHN0dVQ0bzJIWUJSSzN1cGJPcnVQOHZBRGRSVlBZYzVqSlNPVUhZTzZ6VXcrYlhlc2o4SmFkOVBnWSsrcnlaRGZ5TmhCMHBWWTNqUkdwM3RUQXZLQ0hBVnVUdmFsWDZsM0xRV2IxNndOZHdJQ3Jrbk5DTlVSUnVhWFJCSUhRNDltSlM4MHdDTTVQWEx0TlpwMG82NzJGZ3dJb3VETnZxQ1lqIiwibWFjIjoiOTBiZDRlNTg0YTQ2Zjc2NWNhNTIyOWZiOTdmMzgwNmQyZDE5ZTllMzQ0ZWI0OTYxNzA5Y2JkOGVkMmI5NWM2ZiJ9';
        $officer->Status = 1;
        $officer->save();

        $userPermission = new UserPermission;
        $userPermission->UserId = $officer->UserId;
        $userPermission->PermissionKey = 'App:Officer:Profile:Show';
        $userPermission->save();

        $officer = new User;
        $officer->Username = 'Sonicya';
        $officer->Fullname = 'Andrea Adam';
        $officer->NIK = '6543210987654321';
        $officer->NIKConfirmed = 1;
        $officer->NRP = '3216543217654321';
        $officer->RankId = 1;
        $officer->Phone = '6285718866174';
        $officer->Email = 'maxjern8@gmail.com';
        $officer->Source = 'adm';
        $officer->Status = 1;
        $officer->save();
    }
}
