<?php

use App\Role;
use App\User;
use App\UserRole;
use App\RolePermission;
use Illuminate\Database\Seeder;

class RolePermissionsTableSeeder extends Seeder
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Admin' => [
                'Web:Officer:Get',
                'Web:Officer:Create',
                'Web:Officer:Update',
                'Web:Officer:Delete',
                'Web:Officer:Role:Get',
                'Web:Officer:Role:Create',
                'Web:Officer:Role:Update',
                'Web:Officer:Role:Delete',
                'Web:Officer:RolePermission:Get',
                'Web:Officer:RolePermission:Create',
                'Web:Officer:RolePermission:Update',
                'Web:Officer:RolePermission:Delete',
                'Web:Officer:UserRole:Get',
                'Web:Officer:UserRole:Create',
                'Web:Officer:UserRole:Update',
                'Web:Officer:UserRole:Delete',
                'Web:Officer:UserPermission:Get',
                'Web:Officer:UserPermission:Create',
                'Web:Officer:UserPermission:Update',
                'Web:Officer:UserPermission:Delete',
                'Web:Officer:Rank:Create',
                'Web:Officer:Rank:Update',
                'Web:Officer:Rank:Delete',
                'Web:Officer:CustomerData:Get',
                'Web:Officer:CustomerData:Create',
                'Web:Officer:CustomerData:Update',
                'Web:Officer:CustomerData:Delete',
                'Web:Officer:Profile:Update',
                'Web:Officer:Password:Change',
                'Web:Officer:SIM:Search',
                'Web:Officer:SIM:Master:Get',
                'Web:Officer:SIM:Master:Create',
                'Web:Officer:SIM:Master:Update',
                'Web:Officer:SIM:Master:Delete',
                'Web:Officer:GoSend:Booking:Create',
                'Web:Officer:GoSend:Booking:Delete',
            ],
            'Satpas' => [
                'App:Officer:SIM:Validate',
                'App:Officer:SIM:Search',
                'App:Officer:STNK:Validate',
                'App:Officer:TrafficTicket:Create',
                'App:Officer:Password:Change',
                'App:Officer:PIN:Create',
                'App:Officer:PIN:Change',
                'App:Officer:PIN:Forgot',
                'App:Officer:Profile:Get',
                'App:Officer:Profile:Update',
                'App:Officer:Profile:Delete',
            ],
            'Police' => [
                'App:Officer:SIM:Validate',
                'App:Officer:SIM:Search',
                'App:Officer:STNK:Validate',
                'App:Officer:TrafficTicket:Create',
                'App:Officer:Password:Change',
                'App:Officer:PIN:Create',
                'App:Officer:PIN:Change',
                'App:Officer:PIN:Forgot',
                'App:Officer:Profile:Get',
                'App:Officer:Profile:Update',
                'App:Officer:Profile:Delete',
            ],
        ];

        foreach ($roles as $roleName => $rolePermissions) {
            $role = new Role;
            $role->RoleName = $roleName;
            $role->save();

            foreach ($rolePermissions as $permissionKey) {
                $rolePermission = new RolePermission;
                $rolePermission->RoleId = $role->RoleId;
                $rolePermission->PermissionKey = $permissionKey;
                $rolePermission->save();
            }

            $userRole = new UserRole;
            $userRole->UserId = optional(User::where('Username', 'MrAndreID')->first())->UserId;
            $userRole->RoleId = $role->RoleId;
            $userRole->save();
        }

        $userRole = new UserRole;
        $userRole->UserId = optional(User::where('Username', 'Sonicya')->first())->UserId;
        $userRole->RoleId = optional(Role::where('RoleName', 'Police')->first())->RoleId;
        $userRole->save();
    }
}
