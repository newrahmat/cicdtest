<?php

use App\Rank;
use Illuminate\Database\Seeder;

class RanksTableSeeder extends Seeder
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            '001' => 'Bhayangkara Dua (Bharada)',
            '002' => 'Bhayangkara Satu (Bharatu)',
            '003' => 'Bhayangkara Kepala (Baraka)',
            '004' => 'Ajun Brigadir Polisi Dua (Abripda)',
            '005' => 'Ajun Brigadir Polisi Satu (Abriptu)',
            '006' => 'Ajun Brigadir Polisi (Abrip)',
            '007' => 'Brigadir Polisi Dua (Bripda)',
            '008' => 'Brigadir Polisi Satu (Briptu)',
            '009' => 'Brigadir Polisi (Brigpol)',
            '010' => 'Brigadir Polisi Kepala (Bripka)',
            '011' => 'Ajun Inspektur Polisi Dua (Aipda)',
            '012' => 'Ajun Inspektur Polisi Satu (Aiptu)',
            '013' => 'Inspektur Polisi Dua (Ipda)',
            '014' => 'Inspektur Polisi Satu (Iptu)',
            '015' => 'Ajun Komisaris Polisi (AKP)',
            '016' => 'Komisaris Polisi (Kompol)',
            '017' => 'Ajun Komisaris Besar Polisi (AKBP)',
            '018' => 'Komisaris Besar Polisi (Kombes Pol)',
            '019' => 'Brigadir Jenderal Polisi (Brigjen Pol)',
            '020' => 'Inspektur Jenderal Polisi (Irjen Pol)',
            '021' => 'Komisaris Jenderal Polisi (Komjen Pol)',
            '022' => 'Jenderal Polisi (Jenderal Pol)',
        ];

        foreach ($ranks as $rankCode => $rankName) {
            $rank = new Rank;
            $rank->RankCode = $rankCode;
            $rank->RankName = $rankName;
            $rank->RankStatus = 1;
            $rank->save();
        }
    }
}
