<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Users';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'UserId';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "created at" column.
     *
     * @const string
     */
    const CREATED_AT = 'RegisterDate';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "updated at" column.
     *
     * @const string
     */
    const UPDATED_AT = 'LastUpdate';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Username', 'Fullname', 'NIK', 'NIKConfirmed', 'NRP', 'NRPHead', 'RankId', 'Unit', 'Phone', 'PhoneConfirmed', 'Email', 'EmailConfirmed', 'Picture', 'Source', 'PasswordHash', 'PasswordSalt', 'PinHash', 'TwoFactorEnabled', 'AccessFailedCount', 'SecurityStamp', 'InstallId', 'DeviceId', 'DeviceType', 'Status', 'RegisterDate', 'LastUpdate', 'ClosingDate'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'PasswordHash', 'PinHash', 'SecurityStamp',
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Get the rank that owns the user.
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function rank()
    {
        return $this->belongsTo('App\Rank', 'RankId');
    }
}
