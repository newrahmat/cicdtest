<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'UserPermissions';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'UserPermissionId';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "created at" column.
     *
     * @const string
     */
    const CREATED_AT = null;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "updated at" column.
     *
     * @const string
     */
    const UPDATED_AT = null;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UserId', 'PermissionKey'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Get the user that owns the user permission.
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'UserId');
    }
}
