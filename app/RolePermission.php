<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'RolePermissions';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'RolePermissionId';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "created at" column.
     *
     * @const string
     */
    const CREATED_AT = null;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "updated at" column.
     *
     * @const string
     */
    const UPDATED_AT = null;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'RoleId', 'PermissionKey'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Get the role that owns the role permission.
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function role()
    {
        return $this->belongsTo('App\Role', 'RoleId');
    }
}
