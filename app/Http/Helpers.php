<?php

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Make a Response.
 *
 * @param  int  $statusCode
 * @param  string  $status
 * @param  string  $message
 * @param  array|object|null  $data
 * @param  array  $headers
 * @return json
 */
function makeResponse($statusCode, $status, $message, $data = null, $headers = [])
{
    $result = [
        'status_code' => $statusCode,
        'status' => $status == 'pagination' ? 'success' : $status,
        'message' => $message,
        'data' => $data,
    ];

    if ($status == 'pagination') {
        $result = array_merge($result, ['paginator' => [
            'total_records' => (int) $data->total(),
            'total_pages' => (int) $data->lastPage(),
            'current_page' => (int) $data->currentPage(),
            'per_page' => (int) $data->perPage(),
        ]]);
    }

    return response()->json($result, $statusCode, $headers);
}

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Set Status From Request.
 *
 * @param  string  $status
 * @return int
 */
function setStatus($status)
{
    switch ($status) {
        case "active":
            return 1;
        break;

        case "blacklist":
            return 2;
        break;
        
        case "suspend":
            return 3;
        break;
        
        case "expired":
            return 4;
        break;
        
        case "paid":
            return 5;
        break;
        
        case "other":
            return 6;
        break;
        
        case "deleted":
            return 7;
        break;
        
        default:
            return 0;
    }
}

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Get Status.
 *
 * @param  int  $statusCode
 * @return string
 */
function getStatus($statusCode)
{
    switch ($statusCode) {
        case 1:
            return "active";
        break;

        case 2:
            return "blacklist";
        break;
        
        case 3:
            return "suspend";
        break;
        
        case 4:
            return "expired";
        break;
        
        case 5:
            return "paid";
        break;
        
        case 6:
            return "other";
        break;
        
        case 7:
            return "deleted";
        break;
        
        default:
            return "not-active";
    }
}