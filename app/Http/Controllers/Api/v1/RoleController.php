<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\Role as ResourcesRole;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RolePermission;
use App\UserRole;
use App\Role;

class RoleController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @return json
     */
    public function index()
    {
        $roles = Role::all();

        return makeResponse(200, 'success', null, ResourcesRole::collection($roles));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'RoleName' => 'required|max:255',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $role = new Role;
        $role->RoleName = $request->RoleName;
        $role->save();
        
        return makeResponse(201, 'success', 'new role has been save successfully', new ResourcesRole($role));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  int  $id
     * @return json
     */
    public function show($id)
    {
        $role = Role::find($id);

        if (!$role) return makeResponse(404, 'error', 'role not found');
        
        return makeResponse(200, 'success', null, new ResourcesRole($role));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);

        if (!$role) return makeResponse(404, 'error', 'role not found');
        
        $validator = Validator::make($request->all(), [
            'RoleName' => 'required|max:255',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $role->RoleName = $request->RoleName;
        $role->save();

        return makeResponse(200, 'success', 'role has been update successfully', new ResourcesRole($role));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        
        if (!$role) return makeResponse(404, 'error', 'role not found');
        
        if (RolePermission::where('RoleId', $id)->first()) return makeResponse(400, 'error', 'role is being used. please change role in role permission');

        if (UserRole::where('RoleId', $id)->first()) return makeResponse(400, 'error', 'role is being used. please change role in user role');

        $role->delete();

        return makeResponse(200, 'success', 'role has been delete successfully');
    }
}
