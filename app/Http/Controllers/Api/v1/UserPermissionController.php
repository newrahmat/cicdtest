<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\UserPermission as ResourcesUserPermission;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserPermission;

class UserPermissionController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @param  int|null  $userId
     * @return json
     */
    public function index($userId = null)
    {
        $userPermissions = UserPermission::when($userId, function ($query, $userId) {
            return $query->where('UserId', $userId);
        })
        ->get();

        return makeResponse(200, 'success', null, ResourcesUserPermission::collection($userPermissions));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'UserId' => 'required|integer|exists:Users,UserId,ClosingDate,NULL',
            'PermissionKey' => 'required|max:100|unique:UserPermissions,PermissionKey,NULL,UserPermissionId,UserId,' . $request->UserId,
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $userPermission = new UserPermission;
        $userPermission->UserId = $request->UserId;
        $userPermission->PermissionKey = $request->PermissionKey;
        $userPermission->save();
        
        return makeResponse(201, 'success', 'new user permission has been save successfully', new ResourcesUserPermission($userPermission));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  int  $id
     * @return json
     */
    public function show($id)
    {
        $userPermission = UserPermission::find($id);

        if (!$userPermission) return makeResponse(404, 'error', 'user permission not found');
        
        return makeResponse(200, 'success', null, new ResourcesUserPermission($userPermission));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $userPermission = UserPermission::find($id);

        if (!$userPermission) return makeResponse(404, 'error', 'user permission not found');
        
        $validator = Validator::make($request->all(), [
            'UserId' => 'required|integer|exists:Users,UserId,ClosingDate,NULL',
            'PermissionKey' => 'required|max:100|unique:UserPermissions,PermissionKey,' . $userPermission->UserPermissionId . ',UserPermissionId,UserId,' . $request->UserId,
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $userPermission->UserId = $request->UserId;
        $userPermission->PermissionKey = $request->PermissionKey;
        $userPermission->save();

        return makeResponse(200, 'success', 'user permission has been update successfully', new ResourcesUserPermission($userPermission));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $userPermission = UserPermission::find($id);
        
        if (!$userPermission) return makeResponse(404, 'error', 'user permission not found');

        $userPermission->delete();

        return makeResponse(200, 'success', 'user permission has been delete successfully');
    }
}
