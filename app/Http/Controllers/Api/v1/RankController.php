<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\Rank as ResourcesRank;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rank;
use App\User;

class RankController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @return json
     */
    public function index()
    {
        $ranks = Rank::all();

        return makeResponse(200, 'success', null, ResourcesRank::collection($ranks));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'RankCode' => 'nullable|max:255|unique:Ranks,RankCode,NULL,RankId',
            'RankName' => 'required|max:255',
            'RankStatus' => 'required|in:active,not-active',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        if (!isset($request->RankCode)) {
            $lastCode = Rank::orderBy('RankId', 'desc')->first()->RankId;
            $rankCode = '0' . ($lastCode + 1);
        } else {
            $rankCode = $request->RankCode;
        }

        $rank = new Rank;
        $rank->RankCode = $rankCode;
        $rank->RankName = $request->RankName;
        $rank->RankStatus = setStatus($request->RankStatus);
        $rank->save();
        
        return makeResponse(201, 'success', 'new rank has been save successfully', new ResourcesRank($rank));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  int  $id
     * @return json
     */
    public function show($id)
    {
        $rank = Rank::find($id);

        if (!$rank) return makeResponse(404, 'error', 'rank not found');
        
        return makeResponse(200, 'success', null, new ResourcesRank($rank));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $rank = Rank::find($id);

        if (!$rank) return makeResponse(404, 'error', 'rank not found');
        
        $validator = Validator::make($request->all(), [
            'RankCode' => 'nullable|max:255|unique:Ranks,RankCode,' . $rank->RankId . ',RankId',
            'RankName' => 'nullable|max:255',
            'RankStatus' => 'nullable|in:active,not-active',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        if ($request->RankCode) $rank->RankCode = $request->RankCode;
        if ($request->RankName) $rank->RankName = $request->RankName;
        if ($request->RankStatus) $rank->RankStatus = setStatus($request->RankStatus);
        $rank->save();

        return makeResponse(200, 'success', 'rank has been update successfully', new ResourcesRank($rank));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $rank = Rank::find($id);
        
        if (!$rank) return makeResponse(404, 'error', 'rank not found');

        if (User::where('RankId', $id)->first()) return makeResponse(400, 'error', 'rank is being used. please change rank in officer');

        $rank->delete();

        return makeResponse(200, 'success', 'rank has been delete successfully');
    }
}
