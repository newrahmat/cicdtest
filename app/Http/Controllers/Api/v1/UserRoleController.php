<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\UserRole as ResourcesUserRole;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserRole;

class UserRoleController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @param  int|null  $userId
     * @return json
     */
    public function index($userId = null)
    {
        $userRoles = UserRole::when($userId, function ($query, $userId) {
            return $query->where('UserId', $userId);
        })
        ->get();

        return makeResponse(200, 'success', null, ResourcesUserRole::collection($userRoles));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'UserId' => 'required|integer|exists:Users,UserId,ClosingDate,NULL',
            'RoleId' => 'required|integer|exists:Roles,RoleId|unique:UserRoles,RoleId,NULL,UserRoleId,UserId,' . $request->UserId,
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        if (UserRole::where('UserId', $request->UserId)->where('RoleId', $request->RoleId)->first()) return makeResponse(400, 'error', 'user role already exists');

        $userRole = new UserRole;
        $userRole->UserId = $request->UserId;
        $userRole->RoleId = $request->RoleId;
        $userRole->save();
        
        return makeResponse(201, 'success', 'new user role has been save successfully', new ResourcesUserRole($userRole));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  int  $id
     * @return json
     */
    public function show($id)
    {
        $userRole = UserRole::find($id);

        if (!$userRole) return makeResponse(404, 'error', 'user role not found');
        
        return makeResponse(200, 'success', null, new ResourcesUserRole($userRole));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $userRole = UserRole::find($id);

        if (!$userRole) return makeResponse(404, 'error', 'user role not found');
        
        $validator = Validator::make($request->all(), [
            'UserId' => 'required|integer|exists:Users,UserId,ClosingDate,NULL',
            'RoleId' => 'required|integer|exists:Roles,RoleId|unique:UserRoles,RoleId,' . $userRole->UserRoleId . ',UserRoleId,UserId,' . $request->UserId,
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        if (UserRole::where('UserId', $request->UserId)->where('RoleId', $request->RoleId)->where('UserRoleId', '!=', $userRole->UserRoleId)->first()) return makeResponse(400, 'error', 'user role already exists');

        $userRole->UserId = $request->UserId;
        $userRole->RoleId = $request->RoleId;
        $userRole->save();

        return makeResponse(200, 'success', 'user role has been update successfully', new ResourcesUserRole($userRole));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $userRole = UserRole::find($id);
        
        if (!$userRole) return makeResponse(404, 'error', 'user role not found');

        $userRole->delete();

        return makeResponse(200, 'success', 'user role has been delete successfully');
    }
}
