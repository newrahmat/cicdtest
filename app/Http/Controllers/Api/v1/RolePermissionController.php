<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\RolePermission as ResourcesRolePermission;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RolePermission;

class RolePermissionController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @param  int|null  $roleId
     * @return json
     */
    public function index($roleId = null)
    {
        $rolePermissions = RolePermission::when($roleId, function ($query, $roleId) {
            return $query->where('RoleId', $roleId);
        })
        ->get();

        return makeResponse(200, 'success', null, ResourcesRolePermission::collection($rolePermissions));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'RoleId' => 'required|integer|exists:Roles,RoleId',
            'PermissionKey' => 'required|max:100|unique:RolePermissions,PermissionKey,NULL,RolePermissionId,RoleId,' . $request->RoleId,
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $rolePermission = new RolePermission;
        $rolePermission->RoleId = $request->RoleId;
        $rolePermission->PermissionKey = $request->PermissionKey;
        $rolePermission->save();
        
        return makeResponse(201, 'success', 'new role permission has been save successfully', new ResourcesRolePermission($rolePermission));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  int  $id
     * @return json
     */
    public function show($id)
    {
        $rolePermission = RolePermission::find($id);

        if (!$rolePermission) return makeResponse(404, 'error', 'role permission not found');
        
        return makeResponse(200, 'success', null, new ResourcesRolePermission($rolePermission));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $rolePermission = RolePermission::find($id);

        if (!$rolePermission) return makeResponse(404, 'error', 'role permission not found');
        
        $validator = Validator::make($request->all(), [
            'RoleId' => 'required|integer|exists:Roles,RoleId',
            'PermissionKey' => 'required|max:100|unique:RolePermissions,PermissionKey,' . $rolePermission->RolePermissionId . ',RolePermissionId,RoleId,' . $request->RoleId,
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $rolePermission->RoleId = $request->RoleId;
        $rolePermission->PermissionKey = $request->PermissionKey;
        $rolePermission->save();

        return makeResponse(200, 'success', 'role permission has been update successfully', new ResourcesRolePermission($rolePermission));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $rolePermission = RolePermission::find($id);
        
        if (!$rolePermission) return makeResponse(404, 'error', 'role permission not found');

        $rolePermission->delete();

        return makeResponse(200, 'success', 'role permission has been delete successfully');
    }
}
