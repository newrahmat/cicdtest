<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\Officer as OfficerResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class OfficerController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required_with:page|integer|min:1',
            'page' => 'required_with:limit|integer|min:1',
            'search' => 'nullable',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $officers = User::where('Status', '!=', 7)->whereNull('ClosingDate')->when($request->search, function ($query, $key) {
            return $query->where('Fullname', 'like', '%' . $key . '%')
                        ->orWhere('NRP', 'like', '%' . $key . '%')
                        ->orWhere('Phone', 'like', '%' . $key . '%');
        })->paginate($request->limit);

        return makeResponse(200, 'pagination', null, OfficerResource::collection($officers));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function store(Request $request)
    {
        if ($request->Phone) {
            $mobileNumber = preg_replace('/\D/', '', $request->Phone);
            $mobileNumber = preg_replace('/^0/', '62', $mobileNumber);
            $request->merge(['Phone' => $mobileNumber]);
        }

        $validator = Validator::make($request->all(), [
            'Username' => 'nullable|max:100|unique:Users,Username,NULL,UserId,ClosingDate,NULL',
            'Fullname' => 'nullable|max:100',
            'NIK' => 'nullable|max:20|unique:Users,NIK,NULL,UserId,ClosingDate,NULL',
            'NIKConfirmed' => 'nullable|in:0,1',
            'NRP' => 'nullable|max:20|unique:Users,NRP,NULL,UserId,ClosingDate,NULL',
            'NRPHead' => 'nullable|max:20',
            'RankId' => 'nullable|integer|exists:Ranks,RankId',
            'Unit' => 'nullable|max:100',
            'Phone' => 'nullable|numeric|digits_between:6,20|unique:Users,Phone,NULL,UserId,ClosingDate,NULL',
            'PhoneConfirmed' => 'nullable|in:0,1',
            'Email' => 'nullable|email|max:30|unique:Users,Email,NULL,UserId,ClosingDate,NULL',
            'EmailConfirmed' => 'nullable|in:0,1',
            'Picture' => 'nullable|url',
            'Source' => 'nullable|in:adm,web,app',
            'PasswordHash' => 'nullable|max:100',
            'PasswordSalt' => 'nullable',
            'PinHash' => 'nullable|max:100',
            'TwoFactorEnabled' => 'nullable|numeric|digits_between:0,4',
            'AccessFailedCount' => 'nullable|numeric',
            'SecurityStamp' => 'nullable|max:255',
            'InstallId' => 'nullable|max:255',
            'DeviceId' => 'nullable|max:255',
            'DeviceType' => 'nullable|in:android,ios',
            'Status' => 'nullable|in:active,blacklist,suspend,expired,paid,other,not-active',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $officer = new User;
        if ($request->Username) $officer->Username = $request->Username;
        if ($request->Fullname) $officer->Fullname = $request->Fullname;
        if ($request->NIK) $officer->NIK = $request->NIK;
        if (isset($request->NIKConfirmed)) $officer->NIKConfirmed = $request->NIKConfirmed;
        if ($request->NRP) $officer->NRP = $request->NRP;
        if ($request->NRPHead) $officer->NRPHead = $request->NRPHead;
        if ($request->RankId) $officer->RankId = $request->RankId;
        if ($request->Unit) $officer->Unit = $request->Unit;
        if ($request->Phone) $officer->Phone = $request->Phone;
        if (isset($request->PhoneConfirmed)) $officer->PhoneConfirmed = $request->PhoneConfirmed;
        if ($request->Email) $officer->Email = $request->Email;
        if (isset($request->EmailConfirmed)) $officer->EmailConfirmed = $request->EmailConfirmed;
        if ($request->Picture) $officer->Picture = $request->Picture;
        if ($request->Source) $officer->Source = $request->Source;
        if ($request->PasswordHash) $officer->PasswordHash = $request->PasswordHash;
        if ($request->PasswordSalt) $officer->PasswordSalt = $request->PasswordSalt;
        if ($request->PinHash) $officer->PinHash = $request->PinHash;
        if (isset($request->TwoFactorEnabled)) $officer->TwoFactorEnabled = $request->TwoFactorEnabled;
        if (isset($request->AccessFailedCount)) $officer->AccessFailedCount = $request->AccessFailedCount;
        if ($request->SecurityStamp) $officer->SecurityStamp = $request->SecurityStamp;
        if ($request->InstallId) $officer->InstallId = $request->InstallId;
        if ($request->DeviceId) $officer->DeviceId = $request->DeviceId;
        if ($request->DeviceType) $officer->DeviceType = $request->DeviceType;
        if ($request->Status) $officer->Status = setStatus($request->Status);
        $officer->save();
        
        return makeResponse(201, 'success', 'new officer has been save successfully', new OfficerResource($officer));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string  $type
     * @return json
     */
    public function show($id, $type = 'UserId')
    {
        if ($type == 'Email') $id = str_replace('%20', '.', $id);

        $officer = User::where('Status', '!=', 7)->whereNull('ClosingDate')->where($type, $id)->first();

        if (!$officer) return makeResponse(404, 'error', 'officer not found');
        
        return makeResponse(200, 'success', null, new OfficerResource($officer));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        if ($request->Phone) {
            $mobileNumber = preg_replace('/\D/', '', $request->Phone);
            $mobileNumber = preg_replace('/^0/', '62', $mobileNumber);
            $request->merge(['Phone' => $mobileNumber]);
        }

        $officer = User::where('Status', '!=', 7)->whereNull('ClosingDate')->find($id);

        if (!$officer) return makeResponse(404, 'error', 'officer not found');
        
        $validator = Validator::make($request->all(), [
            'Username' => 'nullable|max:100|unique:Users,Username,' . $officer->UserId . ',UserId,ClosingDate,NULL',
            'Fullname' => 'nullable|max:100',
            'NIK' => 'nullable|max:20|unique:Users,NIK,' . $officer->UserId . ',UserId,ClosingDate,NULL',
            'NIKConfirmed' => 'nullable|in:0,1',
            'NRP' => 'nullable|max:20|unique:Users,NRP,' . $officer->UserId . ',UserId,ClosingDate,NULL',
            'NRPHead' => 'nullable|max:20',
            'RankId' => 'nullable|integer|exists:Ranks,RankId',
            'Unit' => 'nullable|max:100',
            'Phone' => 'nullable|numeric|digits_between:6,20|unique:Users,Phone,' . $officer->UserId . ',UserId,ClosingDate,NULL',
            'PhoneConfirmed' => 'nullable|in:0,1',
            'Email' => 'nullable|email|unique:Users,Email,' . $officer->UserId . ',UserId,ClosingDate,NULL',
            'EmailConfirmed' => 'nullable|in:0,1',
            'Picture' => 'nullable|url',
            'Source' => 'nullable|in:adm,web,app',
            'PasswordHash' => 'nullable|max:100',
            'PasswordSalt' => 'nullable',
            'PinHash' => 'nullable|max:100',
            'TwoFactorEnabled' => 'nullable|numeric|digits_between:0,4',
            'AccessFailedCount' => 'nullable|numeric',
            'SecurityStamp' => 'nullable|max:255',
            'InstallId' => 'nullable|max:255',
            'DeviceId' => 'nullable|max:255',
            'DeviceType' => 'nullable|in:android,ios',
            'Status' => 'nullable|in:active,blacklist,suspend,expired,paid,other,not-active',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        if ($request->Username) $officer->Username = $request->Username;
        if ($request->Fullname) $officer->Fullname = $request->Fullname;
        if ($request->NIK) $officer->NIK = $request->NIK;
        if (isset($request->NIKConfirmed)) $officer->NIKConfirmed = $request->NIKConfirmed;
        if ($request->NRP) $officer->NRP = $request->NRP;
        if ($request->NRPHead) $officer->NRPHead = $request->NRPHead;
        if ($request->RankId) $officer->RankId = $request->RankId;
        if ($request->Unit) $officer->Unit = $request->Unit;
        if ($request->Phone) $officer->Phone = $request->Phone;
        if (isset($request->PhoneConfirmed)) $officer->PhoneConfirmed = $request->PhoneConfirmed;
        if ($request->Email) $officer->Email = $request->Email;
        if (isset($request->EmailConfirmed)) $officer->EmailConfirmed = $request->EmailConfirmed;
        if ($request->Picture) $officer->Picture = $request->Picture;
        if ($request->Source) $officer->Source = $request->Source;
        if ($request->PasswordHash) $officer->PasswordHash = $request->PasswordHash;
        if ($request->PasswordSalt) $officer->PasswordSalt = $request->PasswordSalt;
        if ($request->PinHash) $officer->PinHash = $request->PinHash;
        if (isset($request->TwoFactorEnabled)) $officer->TwoFactorEnabled = $request->TwoFactorEnabled;
        if (isset($request->AccessFailedCount)) $officer->AccessFailedCount = $request->AccessFailedCount;
        if ($request->SecurityStamp) $officer->SecurityStamp = $request->SecurityStamp;
        if ($request->InstallId) $officer->InstallId = $request->InstallId;
        if ($request->DeviceId) $officer->DeviceId = $request->DeviceId;
        if ($request->DeviceType) $officer->DeviceType = $request->DeviceType;
        if ($request->Status) $officer->Status = setStatus($request->Status);
        $officer->save();

        return makeResponse(200, 'success', 'officer has been update successfully', new OfficerResource($officer));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $officer = User::where('Status', '!=', 7)->whereNull('ClosingDate')->find($id);
        
        if (!$officer) return makeResponse(404, 'error', 'officer not found');

        $officer->status = setStatus('deleted');
        $officer->ClosingDate = date('Y-m-d H:i:s');
        $officer->save();

        return makeResponse(200, 'success', 'officer has been delete successfully');
    }
}
