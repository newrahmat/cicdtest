<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RolePermission extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [
            'Id' => $this->RolePermissionId,
            'PermissionKey' => $this->PermissionKey,
            'Role' => new Role($this->role),
        ];

        return $result;
    }
}