<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Officer extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [
            'Id' => $this->UserId,
            'Username' => $this->Username,
            'Fullname' => $this->Fullname,
            'NIK' => $this->NIK,
            'NIKConfirmed' => (bool) $this->NIKConfirmed,
            'NRP' => $this->NRP,
            'NRPHead' => $this->NRPHead,
            'Rank' => new Rank($this->rank),
            'Unit' => $this->Unit,
            'Phone' => $this->Phone,
            'PhoneConfirmed' => (bool) $this->PhoneConfirmed,
            'Email' => $this->Email,
            'EmailConfirmed' => (bool) $this->EmailConfirmed,
            'Picture' => $this->Picture,
            'Source' => $this->Source,
            'PasswordHash' => $this->PasswordHash,
            'PasswordSalt' => $this->PasswordSalt,
            'PinHash' => $this->PinHash,
            'TwoFactorEnabled' => $this->TwoFactorEnabled,
            'AccessFailedCount' => $this->AccessFailedCount,
            'SecurityStamp' => $this->SecurityStamp,
            'InstallId' => $this->InstallId,
            'DeviceId' => $this->DeviceId,
            'DeviceType' => $this->DeviceType,
            'Status' => getStatus($this->Status),
            'RegisterDate' => date('Y-m-d H:i:s', strtotime($this->RegisterDate)),
            'LastUpdate' => date('Y-m-d H:i:s', strtotime($this->LastUpdate)),
            'ClosingDate' => $this->ClosingDate != null ? date('Y-m-d H:i:s', strtotime($this->ClosingDate)) : null,
        ];

        return $result;
    }
}