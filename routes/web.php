<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Route for Documentation.
 */
$router->get('/', function () {
    return redirect('https://documenter.getpostman.com/view/5385605/SzYZ1Jay');
});

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Route Group for API.
 */
$router->group(['prefix' => 'api', 'namespace' => 'Api', 'middleware' => 'api'], function () use ($router) {
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Route Group for Version 1.
     */
    $router->group(['prefix' => 'v1', 'namespace' => 'v1'], function () use ($router) {
        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for Officer.
         */
        $router->get('officer', [
            'as' => 'officer.index', 'uses' => 'OfficerController@index'
        ]);
        $router->post('officer', [
            'as' => 'officer.store', 'uses' => 'OfficerController@store'
        ]);
        $router->get('officer/{id}[/{type}]', [
            'as' => 'officer.show', 'uses' => 'OfficerController@show'
        ]);
        $router->put('officer/{id}', [
            'as' => 'officer.update', 'uses' => 'OfficerController@update'
        ]);
        $router->delete('officer/{id}', [
            'as' => 'officer.destroy', 'uses' => 'OfficerController@destroy'
        ]);

        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for Role.
         */
        $router->get('role', [
            'as' => 'role.index', 'uses' => 'RoleController@index'
        ]);
        $router->post('role', [
            'as' => 'role.store', 'uses' => 'RoleController@store'
        ]);
        $router->get('role/{id}', [
            'as' => 'role.show', 'uses' => 'RoleController@show'
        ]);
        $router->put('role/{id}', [
            'as' => 'role.update', 'uses' => 'RoleController@update'
        ]);
        $router->delete('role/{id}', [
            'as' => 'role.destroy', 'uses' => 'RoleController@destroy'
        ]);

        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for Role Permission.
         */
        $router->get('role-permission[/{roleId}/role]', [
            'as' => 'role-permission.index', 'uses' => 'RolePermissionController@index'
        ]);
        $router->post('role-permission', [
            'as' => 'role-permission.store', 'uses' => 'RolePermissionController@store'
        ]);
        $router->get('role-permission/{id}', [
            'as' => 'role-permission.show', 'uses' => 'RolePermissionController@show'
        ]);
        $router->put('role-permission/{id}', [
            'as' => 'role-permission.update', 'uses' => 'RolePermissionController@update'
        ]);
        $router->delete('role-permission/{id}', [
            'as' => 'role-permission.destroy', 'uses' => 'RolePermissionController@destroy'
        ]);

        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for User Permission.
         */
        $router->get('user-permission[/{userId}/user]', [
            'as' => 'user-permission.index', 'uses' => 'UserPermissionController@index'
        ]);
        $router->post('user-permission', [
            'as' => 'user-permission.store', 'uses' => 'UserPermissionController@store'
        ]);
        $router->get('user-permission/{id}', [
            'as' => 'user-permission.show', 'uses' => 'UserPermissionController@show'
        ]);
        $router->put('user-permission/{id}', [
            'as' => 'user-permission.update', 'uses' => 'UserPermissionController@update'
        ]);
        $router->delete('user-permission/{id}', [
            'as' => 'user-permission.destroy', 'uses' => 'UserPermissionController@destroy'
        ]);

        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for User Role.
         */
        $router->get('user-role[/{userId}/user]', [
            'as' => 'user-role.index', 'uses' => 'UserRoleController@index'
        ]);
        $router->post('user-role', [
            'as' => 'user-role.store', 'uses' => 'UserRoleController@store'
        ]);
        $router->get('user-role/{id}', [
            'as' => 'user-role.show', 'uses' => 'UserRoleController@show'
        ]);
        $router->put('user-role/{id}', [
            'as' => 'user-role.update', 'uses' => 'UserRoleController@update'
        ]);
        $router->delete('user-role/{id}', [
            'as' => 'user-role.destroy', 'uses' => 'UserRoleController@destroy'
        ]);

        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for Rank.
         */
        $router->get('rank', [
            'as' => 'rank.index', 'uses' => 'RankController@index'
        ]);
        $router->post('rank', [
            'as' => 'rank.store', 'uses' => 'RankController@store'
        ]);
        $router->get('rank/{id}', [
            'as' => 'rank.show', 'uses' => 'RankController@show'
        ]);
        $router->put('rank/{id}', [
            'as' => 'rank.update', 'uses' => 'RankController@update'
        ]);
        $router->delete('rank/{id}', [
            'as' => 'rank.destroy', 'uses' => 'RankController@destroy'
        ]);
    });
});
