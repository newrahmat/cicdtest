<?php

class UserPermissionTest extends TestCase
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Initial for URL.
     *
     * @var string
     */
    private $initURL = 'api/v1/';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Primary URL.
     *
     * @var string
     */
    private $primaryURL = 'user-permission/';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Headers.
     *
     * @var array
     */
    private $header = ['Accept' => 'application/json'];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Data.
     *
     * @var array
     */
    private $data = [
        'Id',
        'PermissionKey',
        'User' => [
            'Id',
            'Username',
            'Fullname',
            'NIK',
            'NIKConfirmed',
            'NRP',
            'NRPHead',
            'Rank' => [
                'Id',
                'Code',
                'Name',
                'Status',
            ],
            'Unit',
            'Phone',
            'PhoneConfirmed',
            'Email',
            'EmailConfirmed',
            'Source',
            'PasswordHash',
            'PasswordSalt',
            'PinHash',
            'TwoFactorEnabled',
            'AccessFailedCount',
            'SecurityStamp',
            'InstallId',
            'DeviceId',
            'DeviceType',
            'Status',
            'RegisterDate',
            'LastUpdate',
            'ClosingDate'
        ]
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Body.
     *
     * @var array
     */
    private $body = [
        'UserId' => 2,
        'PermissionKey' => 'App:Member:PIN:Check'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resources.
     *
     * @return void
     */
    public function testGetAllResources()
    {
        $this->get($this->initURL . $this->primaryURL, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => [
                $this->data
            ]
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @return void
     */
    public function testCreateResource()
    {
        $this->post($this->initURL . $this->primaryURL, $this->body, $this->header);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @return void
     */
    public function testGetResource()
    {
        $this->get($this->initURL . $this->primaryURL . 2, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @return void
     */
    public function testUpdateResource()
    {
        $this->put($this->initURL . $this->primaryURL . 2, $this->body, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @return void
     */
    public function testDeleteResource()
    {
        $this->delete($this->initURL . $this->primaryURL . 2, [], $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
    }
}
