<?php

class RankTest extends TestCase
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Initial for URL.
     *
     * @var string
     */
    private $initURL = 'api/v1/';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Primary URL.
     *
     * @var string
     */
    private $primaryURL = 'rank/';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Headers.
     *
     * @var array
     */
    private $header = ['Accept' => 'application/json'];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Data.
     *
     * @var array
     */
    private $data = [
        'Id',
        'Code',
        'Name',
        'Status'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Body.
     *
     * @var array
     */
    private $body = [
        'RankCode' => '023',
        'RankName' => 'AA',
        'RankStatus' => 'active'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resources.
     *
     * @return void
     */
    public function testGetAllResources()
    {
        $this->get($this->initURL . $this->primaryURL, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => [
                $this->data
            ]
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @return void
     */
    public function testCreateResource()
    {
        $this->post($this->initURL . $this->primaryURL, $this->body, $this->header);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @return void
     */
    public function testGetResource()
    {
        $this->get($this->initURL . $this->primaryURL . 23, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @return void
     */
    public function testUpdateResource()
    {
        $this->put($this->initURL . $this->primaryURL . 23, $this->body, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @return void
     */
    public function testDeleteResource()
    {
        $this->delete($this->initURL . $this->primaryURL . 23, [], $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
    }
}
