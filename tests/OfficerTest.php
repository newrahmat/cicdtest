<?php

class OfficerTest extends TestCase
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Initial for URL.
     *
     * @var string
     */
    private $initURL = 'api/v1/';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Primary URL.
     *
     * @var string
     */
    private $primaryURL = 'officer/';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Headers.
     *
     * @var array
     */
    private $header = ['Accept' => 'application/json'];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Data.
     *
     * @var array
     */
    private $data = [
        'Id',
        'Username',
        'Fullname',
        'NIK',
        'NIKConfirmed',
        'NRP',
        'NRPHead',
        'Rank' => [
            'Id',
            'Code',
            'Name',
            'Status',
        ],
        'Unit',
        'Phone',
        'PhoneConfirmed',
        'Email',
        'EmailConfirmed',
        'Picture',
        'Source',
        'PasswordHash',
        'PasswordSalt',
        'PinHash',
        'TwoFactorEnabled',
        'AccessFailedCount',
        'SecurityStamp',
        'InstallId',
        'DeviceId',
        'DeviceType',
        'Status',
        'RegisterDate',
        'LastUpdate',
        'ClosingDate'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Body.
     *
     * @var array
     */
    private $body = [
        'Username' => 'mrandreid21',
        'Fullname' => 'Andrea Adam',
        'NIK' => '12367890654321',
        'NIKConfirmed' => 1,
        'NRP' => '6543210987654321',
        'NRPHead' => '5432100987654321',
        'RankId' => 15,
        'Unit' => 'AA',
        'Phone' => '081111111111',
        'PhoneConfirmed' => 1,
        'Email' => 'andreaadam215@gmail.com',
        'EmailConfirmed' => 1,
        'Picture' => 'https://mrandreid.com/',
        'Source' => 'adm',
        'PasswordHash' => '123123',
        'PasswordSalt' => '123123',
        'PinHash' => '123456',
        'TwoFactorEnabled' => 0,
        'AccessFailedCount' => 1,
        'SecurityStamp' => 'Andr344dam',
        'InstallId' => '22ac60ec-8afe-4400-a90c-c7c2bb320c14',
        'DeviceId' => 'Device ID for Test',
        'DeviceType' => 'android',
        'Status' => 'active',
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resources.
     *
     * @return void
     */
    public function testGetAllResources()
    {
        $this->get($this->initURL . $this->primaryURL, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => [
                $this->data
            ]
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @return void
     */
    public function testCreateResource()
    {
        $this->post($this->initURL . $this->primaryURL, $this->body, $this->header);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @return void
     */
    public function testGetResource()
    {
        $this->get($this->initURL . $this->primaryURL . 3, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @return void
     */
    public function testUpdateResource()
    {
        $this->put($this->initURL . $this->primaryURL . 3, $this->body, $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data' => $this->data
        ]);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @return void
     */
    public function testDeleteResource()
    {
        $this->delete($this->initURL . $this->primaryURL . 3, [], $this->header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
    }
}
