#{1} = BRANCH_NAME
#{2} = APP_NAME
#{3} = DOCKER_REPO
#{4} = DOCKER_IMAGE
#{5} = VERSION_TAG
#{6} = APP_PORT
#{7} = SERVER_USER
#{8} = SERVER_HOST
#{9} = DOCKER_USER
#{10}= DOCKER_PASSWORD 
#setup.sh $(BRANCH_NAME) $(APP_NAME) $(DOCKER_REPO) $(DOCKER_IMAGE) $(VERSION_TAG) $(APP_PORT) $(SERVER_USER) $(SERVER_HOST)$(DOCKER_USER) $(DOCKER_PASSWORD) 
mkdir -p deploy/${1}-${2};
cp .env.${1} .env
cat <<EOF1>ci/unit_test.sh
docker run --rm ${3}/${4}:${5} sh -c "rm .env && cp .env.test .env && php artisan migrate:fresh --seed --force && ./vendor/bin/phpunit" > result.txt
EOF1

cat <<EOF2>ci/Dockerfile
FROM php:7.4-fpm-alpine

Maintainer Mamat Nurahmat <mamat@loyalto.id>

RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN apk --update add wget \ 
		     curl \
		     git \
		     php7 \
		     php7-curl \
		     php7-openssl \
		     php7-iconv \
		     php7-json \
		     php7-mbstring \
		     php7-phar \
		     php7-dom --repository http://nl.alpinelinux.org/alpine/edge/testing/ && rm /var/cache/apk/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer 

RUN mkdir -p /var/www

WORKDIR /var/www

COPY . .

RUN composer install --optimize-autoloader

CMD php -S 0.0.0.0:${6} -t public

EXPOSE ${6}
EOF2


cat <<EOF3>deploy/${1}-${2}/.env
APP_IMAGE=${3}/${4}:${5}
APP_PORT=${6}
EOF3

cat <<"EOF4">deploy/${1}-${2}/docker-compose.yaml
version: '2'
services:
  app:
    image: ${APP_IMAGE}
    ports:
      - "${APP_PORT}:${APP_PORT}"
    mem_limit: 128MB
    mem_reservation: 64MB
    network_mode: host
    restart: always
EOF4

cat <<EOF5>deploy/deploy.sh
scp -prq deploy/${1}-${2}/ ${7}@${8}:~/deploy/
ssh ${7}@${8} << EOF1
docker login -u ${9} -p'${10}'
EOF1

ssh ${7}@${8} << EOF2
cp deploy/${1}-${2}/.env deploy/${1}-${2}/rollback.env;  
cd deploy/${1}-${2};
docker-compose up -d --force-recreate
EOF2
EOF5

