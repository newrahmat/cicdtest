GIT_BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

if [ $GIT_BRANCH_NAME = "master" ]
then
        TAG_VERSION=$(git describe --tags $(git rev-list --tags --max-count=1))
elif [ $GIT_BRANCH_NAME = "staging" ]
then
	ENV=$(git rev-parse --abbrev-ref HEAD)
	TAG_VERSION=$ENV-$(date +%F)-$(git rev-parse --short HEAD)
else
        ENV=$(git rev-parse --abbrev-ref HEAD)
        TAG_VERSION=$ENV-$(date +%F)-$(git rev-parse --short HEAD)
fi

echo $TAG_VERSION
